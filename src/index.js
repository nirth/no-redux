// Let's recap basics of the pure functions and differences from closures.
require('./pure-functions');

// Example of why pure functions are very useful, and map reduce.
require('./map-reduce');

// Let's create very simple app state, and use reduce to update it as actions arrive.
require('./simple-app-state');
